
import glob
import select
import sys
import os, shutil
from threading import Thread
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import QColor
from PyQt5 import QtCore
from PyQt5.QtCore import QEvent, QObject, Qt, pyqtSignal, QRegExp
from api import sum_api
from api.gpt_api import *
from api.diff_api import *
from api.sum_api import *
from connection import client
from api import tran_api
# API_TOKEN = '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'
# API_URL = "https://api-inference.huggingface.co/models/"
# headers = {"Authorization": f"Bearer {API_TOKEN}"}
class MainWin(QMainWindow):

    def __init__(self,parent = None):
        # self.central_widget = QtGui.QStackedWidget()
        # self.setCentralWidget(self.central_widget)
        # login_widget = LoginWidget(self)
        # login_widget.button.clicked.connect(self.login)
        # self.central_widget.addWidget(login_widget)
        super(MainWin,self).__init__(parent)
        self.setWindowTitle('=====>Story_gener<=====')
        self.setGeometry(100,100,1500,800)
        self.central_widget = QStackedWidget()
        self.setCentralWidget(self.central_widget)
        self.sum_list = []
        self.gpt_prompt = gpt_prompt(self)
        self.central_widget.addWidget(self.gpt_prompt)
        self.win_p = self.palette()
        self.win_p.setColor(QPalette.Window, QColor("#161418"))
        self.setPalette(self.win_p)
        f1 = QFont('Comic Sans MS',14)
        
        

        self.setStyleSheet("""
            QMenuBar {
                background-color: rgb(49,49,49);
                color: rgb(255,255,255);
                border: 1px solid #000;
            }

            QMenuBar::item {
                background-color: rgb(49,49,49);
                color: rgb(255,255,255);
            }

            QMenuBar::item::selected {
                background-color: rgb(30,30,30);
            }

            QMenu {
                background-color: rgb(49,49,49);
                color: rgb(255,255,255);
                border: 1px solid #000;           
            }

            QMenu::item::selected {
                background-color: rgb(30,30,30);
            }
            """
        )

       

        button_action = QAction("&check others galary", self)
        button_action.setStatusTip("This is your button")
        button_action.triggered.connect(self.lookup_share_event)
        

        menu = self.menuBar()

        file_menu = menu.addMenu("&Galary")
        file_menu.addAction(button_action)
    
    def lookup_share_event(self,s):
        c.galary_name_list = []
        self.share_win = Share_win()
        self.share_win.show()
        
        
    def diff_gen_event(self):
        
        
        
        self.display_img = display_img(self)
        self.central_widget.addWidget(self.display_img)
        self.central_widget.setCurrentWidget(self.display_img)
        
    def gpt_gen_btn_event(self):
        c.GPT_out_state = False
        self.display_story = display_story(self)
        self.display_story.prompt = self.gpt_prompt.prompt_box.text()
        self.central_widget.addWidget(self.display_story)
        self.central_widget.setCurrentWidget(self.display_story)
        
    def go_back_event_story(self):
        
        self.central_widget.removeWidget(self.display_story)
    def go_back_event_img(self):
        self.central_widget.removeWidget(self.display_img)
    
    def get_slider_len(self):
        return self.gpt_prompt.gpt_len_slide_value.text()
    
class display_img(QWidget):
    next_signal = pyqtSignal()
    prv_signal = pyqtSignal()
    go_back_signal = pyqtSignal()
    def __init__(self, parent = None):
        super(display_img,self).__init__(parent)
        self.next_signal.connect(self.next_image_event)
        self.go_back_signal.connect(self.parent().go_back_event_img)
        self.prv_signal.connect(self.prv_image_event)
        self.pre_img_btn = QPushButton('Previous')
        self.pre_img_btn.setFixedSize(150,30)
        self.pre_img_btn.hide()
        
        self.num_process_slider_value_name = QLabel('    num_process')
        self.num_process_slider_value = QLabel("100")
        self.guidance_scale_slider_value_name = QLabel('    guidance_scale')
        self.guidance_scale_slider_value = QLabel("7.5")
        
        
        self.num_process_slider = QSlider()
        self.num_process_slider.setOrientation(1)
        self.num_process_slider.setMaximum(400)
        self.num_process_slider.setMinimum(50)
        self.num_process_slider.setValue(100)
        self.num_process_slider.setSingleStep(1)
        self.num_process_slider.valueChanged.connect(self.num_process_change_event)

        
        self.guidance_scale_slider = QSlider()
        self.guidance_scale_slider.setOrientation(1)
        self.guidance_scale_slider.setMaximum(40)
        self.guidance_scale_slider.setMinimum(1)
        self.guidance_scale_slider.setSingleStep(1)
        self.guidance_scale_slider.setValue(15)
        self.guidance_scale_slider.valueChanged.connect(self.guidance_scale_slider_change_event)

        self.nxt_img_btn = QPushButton('Next')
        self.nxt_img_btn.setFixedSize(150,30)
        self.nxt_img_btn.hide()
        
        self.back_btn = QPushButton('go back')
        self.space = QLabel('        ')
        self.current_diff_model_id = 'CompVis/stable-diffusion-v1-4'
        
        self.diff_gen_btn=QPushButton('Request Images')
        self.diff_gen_btn.setFixedSize(200,40)
        self.diff_gen_btn.clicked.connect(self.diff_gen_req)
        
        # self.init_diff_req_btn = QPushButton('INIT DIFF')
        # self.init_diff_req_btn.setFixedSize(300,60)
        # self.init_diff_req_btn.clicked.connect(self.init_diff_req)
        self.translate_btn = QPushButton('Translate')
        self.translate_btn.setFixedSize(150,30)
        self.translate_btn.clicked.connect(self.tran_translate)
        
        self.tran_show_btn = QPushButton('show tran')
        self.tran_show_btn.setFixedSize(150,30)
        self.tran_show_btn.clicked.connect(self.tran_show)
        
        self.diff_gen_check_label = QLabel()
        self.diff_gen_check_label.setFixedSize(20,20)
        
        self.diff_select_box = QComboBox()
        self.diff_select_box.addItems([
            'CompVis/stable-diffusion-v1-4',
            'runwayml/stable-diffusion-v1-5',
            'brathief/Alice_extend_brathief_e500',
            'brathief/Alice_extend_brathief_e100',
            'brathief/wwoo_1000_lora',
            'Smoden/newest_Alice_diff_lora',
            'Smoden/newest_wizard_of_oz_diff_lora',
            'Smoden/Chronicles_diff_lora_6000',
            'Smoden/newest_Grimm_diff_lora',
        ])
        self.diff_select_box.setFixedSize(200,30)
        self.diff_select_box.setCurrentIndex(0)
        self.diff_select_box.currentIndexChanged.connect(self.change_diff_event)
        
        self.save_local_btn = QPushButton('>save to local<')
        self.save_local_btn.setFixedSize(200,40)
        self.save_local_btn.clicked.connect(self.save_local_event)
        
        self.share_btn = QPushButton('>share<')
        self.share_btn.setFixedSize(200,40)
        self.share_btn.clicked.connect(self.share_event)
        
        self.diff_check_btn = QPushButton('check')
        self.diff_check_btn.setFixedSize(200,40)
        self.diff_check_btn.clicked.connect(self.diff_check_image)
        
        self.diff_show_btn = QPushButton('show images')
        self.diff_show_btn.setFixedSize(512,40)
        self.diff_show_btn.clicked.connect(self.init_images)
        
        self.image_prompt = QTextEdit()
        self.image_prompt.setFixedSize(512,100)
        
        self.special_prompt = QTextEdit()
        self.special_prompt.setFixedSize(300,512)
        self.special_prompt.setText(
            '''
            8k,
            sunlight,
            highly quality,
            masterpiece,
            cartoonic,
            Highly detailed,
            Atmosphere,
            Intricate details,
            colorful,
            colorful,
            vibrant and vivid,
            smooth,
            high contrast,
            magical,
            neon,
            Pixar style ,
            pastel colors,
            sunshine,
            dream,
            detailed rendering,
            Sharp focus,
            surreal,
            insanely intricate and detailed,
            unreal engine,
            high definition,
            ultra realistic,	 
            hyperrealistic, 
            extreme details, 	
            highly detailed
            cinematic, 
            masterpiece
            Studio Ghibli,
            anime,
            sunset,
            beautiful light,
            full of colour,
            cinematic lighting,

            
            '''
        )
        self.image_out = QLabel()
        self.image_out.setFixedSize(512,512)
        self.nxt_img_btn.clicked.connect(lambda:self.next_signal.emit())
        self.pre_img_btn.clicked.connect(lambda:self.prv_signal.emit())
        self.back_btn.clicked.connect(lambda:self.go_back_signal.emit())
        self.image_list = []
        self.image_path = 'client_out'
        self.local_image_path = 'client_galary'
        self.image_idx = 0
        self.sum_list = []
        #self.image_out.setFixedSize(512,512)
        f = QFont('Comic Sans MS',14)
        f2 = QFont('Comic Sans MS',8)
        self.diff_select_box.setFont(f)
        self.save_local_btn.setFont(f)
        self.share_btn.setFont(f)
        self.diff_gen_btn.setFont(f)
        self.diff_check_btn.setFont(f)
        self.image_prompt.setFont(f2)
        self.special_prompt.setFont(f2)
        self.pre_img_btn.setFont(f)
        self.nxt_img_btn.setFont(f)
        self.diff_show_btn.setFont(f)
        self.num_process_slider_value_name.setFont(f)
        self.guidance_scale_slider_value_name.setFont(f)
        self.guidance_scale_slider_value.setFont(f2)
        self.num_process_slider_value.setFont(f2)
        self.back_btn.setFont(f)
        self.tran_show_btn.setFont(f2)
        lay = QGridLayout()
        lay.addWidget(self.diff_select_box,0,0)
        lay.addWidget(self.save_local_btn,0,5)
        lay.addWidget(self.share_btn,0,6)
        
        lay.addWidget(self.diff_gen_btn,1,0)
        lay.addWidget(self.space,1,1)
        #lay.addWidget(self.diff_check_btn,2,0)
        
        lay.addWidget(self.num_process_slider_value_name,3,0)
        lay.addWidget(self.num_process_slider,4,0)
        lay.addWidget(self.num_process_slider_value,4,1)
        lay.addWidget(self.guidance_scale_slider_value_name,5,0)
        lay.addWidget(self.guidance_scale_slider,6,0)
        lay.addWidget(self.guidance_scale_slider_value,6,1)
        
        lay.addWidget(self.image_prompt,1,4)
        lay.addWidget(self.translate_btn,1,5)
        lay.addWidget(self.tran_show_btn,1,6)
        lay.addWidget(self.image_out,2,4)
        lay.addWidget(self.special_prompt,2,2)
        lay.addWidget(self.pre_img_btn,3,3)
        lay.addWidget(self.nxt_img_btn,3,5)
        lay.addWidget(self.space,3,5)
        lay.addWidget(self.diff_show_btn,4,4,1,3)
        lay.addWidget(self.back_btn,7,0)
        
        self.setLayout(lay)
        
        self.num_process_slider_value_name.setStyleSheet(
                    "background-color:#0e1315;\
                    color: #15B5B0;\
                    font-weight: solid;"
        )
        
        self.guidance_scale_slider_value_name.setStyleSheet(
                    "background-color:#0e1315;\
                    color: #15B5B0;\
                    font-weight: solid;"
        )
        
        self.guidance_scale_slider_value.setStyleSheet(
            "color: #FF3660;"
        )

        self.num_process_slider_value.setStyleSheet(
            "color: #FF3660;"
        )

        self.guidance_scale_slider.setStyleSheet(
            '''
            QSlider {
                border-radius: 10px;
            }
            QSlider::groove:horizontal {
                height: 5px;
                background: #000;
            }
            QSlider::handle:horizontal{
                background: #f00;
                width: 16px;
                height: 16px;
                margin:-6px 0;
                border-radius:8px;
            }
            QSlider::sub-page:horizontal{
                background:#f90;
            }
            '''
        )

        self.num_process_slider.setStyleSheet(
            '''
            QSlider {
                border-radius: 10px;
            }
            QSlider::groove:horizontal {
                height: 5px;
                background: #000;
            }
            QSlider::handle:horizontal{
                background: #f00;
                width: 16px;
                height: 16px;
                margin:-6px 0;
                border-radius:8px;
            }
            QSlider::sub-page:horizontal{
                background:#f90;
            }
            '''
        )


        self.translate_btn.setStyleSheet(
                    "background-color:#0e1315;\
                    color: #15B5B0;\
                    font-weight: bold;"
        )
        
        self.tran_show_btn.setStyleSheet(
                    "background-color:#0e1315;\
                    color: #15B5B0;\
                    font-weight: bold;"
        )
        self.special_prompt.setStyleSheet(
                    "background-color: #48D8D8;\
                    color: #000000;\
                    font: bold 14px;"                   
        )
        self.image_prompt.setStyleSheet(
                    "background-color: #F15152;\
                    color: #d4d4d4;\
                    font: bold 14px;"                   
        )
        
        self.diff_select_box.setStyleSheet(
                    "background-color:#0e1315;\
                    color: #15B5B0;"
        )
        
        self.back_btn.setStyleSheet(
                    "background-color:#0e1315;\
                    color: #15B5B0;\
                    font-weight: bold;"
        ) 
        self.nxt_img_btn.setStyleSheet(
                    "background-color:#0e1315;\
                    color: #15B5B0;\
                    font-weight: bold;"
        ) 
        self.pre_img_btn.setStyleSheet(
                    "background-color:#0e1315;\
                    color: #15B5B0;\
                    font-weight: bold;"
        )
        self.diff_gen_btn.setStyleSheet(
                    "background-color:#0e1315;\
                    color: #15B5B0;\
                    font-weight: bold;"
        )
        self.diff_check_btn.setStyleSheet(
                    "background-color:#0e1315;\
                    color: #15B5B0;\
                    font-weight: bold;"
        )
        self.diff_show_btn.setStyleSheet(
                    "background-color:#0e1315;\
                    color: #15B5B0;\
                    font-weight: bold;"
        )
        self.save_local_btn.setStyleSheet(
                    "background-color:#0e1315;\
                    color: #15B5B0;\
                    font-weight: bold;"
        )
        self.share_btn.setStyleSheet(
                    "background-color:#0e1315;\
                    color: #15B5B0;\
                    font-weight: bold;"
        )
        
        self.diff_gen_check_label.setStyleSheet(
                    "background-color:#9c142d;\
                    color: #15B5B0;"
        ) if c.diff_state != True else self.diff_gen_check_label.setStyleSheet(
                        "background-color:#1ff177;\
                        color: #15B5B0;"
        )
    
    
    def guidance_scale_slider_change_event(self):
        self.guidance_scale_slider_value.setText(str(float(self.guidance_scale_slider.value()/2)))


    def num_process_change_event(self):
        self.num_process_slider_value.setText(str(self.num_process_slider.value()))

    
    def tran_translate(self):
        
        if len(self.sum_list) > 0 and c.tran_state:
            print('req for translate')
            c.s.send(f'TRAN_GEN|{self.sum_list[self.image_idx]}|{self.image_idx}'.encode())

    def tran_show(self):
        self.image_prompt.setText(f'{self.sum_list[self.image_idx]}\n{c.translated_text}')
        
    def share_event(self):
        c.s.send(f'UPLOAD|{c.user_name}'.encode())
        
    def save_local_event(self):
        for file in os.listdir(self.image_path):
            f = os.path.join(self.image_path, file)
            if os.path.isfile(f):
                shutil.copy2(f, self.local_image_path)
        print('local save done')
        
    def diff_check_image(self):
        if c.diff_gen_state == True:
            self.diff_gen_check_label.setStyleSheet(
                    "background-color:#1ff177;\
                    color: #15B5B0;"
            )
            
    def change_diff_event(self):
        self.current_diff_model_id = self.diff_select_box.currentText()
        print('current diff model id: ',self.current_diff_model_id)
        
    def diff_gen_req(self):
        c.diff_gen_state = False
        c.images_num = 0
        c.s.sendall('SUM_GEN|'.encode())
        
        files = glob.glob(f'{self.image_path}/*.png')
        for f in files:
            os.remove(f)
        
        num = self.num_process_slider_value.text().strip()
        gui = self.guidance_scale_slider_value.text().strip()
        
        c.s.send(
            f'DIFF_GEN|{self.current_diff_model_id}|{self.special_prompt.toPlainText()}|{int(num)}|{float(gui)}'.encode()
        )
        
    def init_images(self):
        if c.diff_gen_state == True:

            self.image_idx = 0
            self.image_list = []
            try:
                f = open(f'{self.image_path}/story.txt', 'r', encoding='utf-8')
            except Exception as e:
                print(e)
            self.sum_list = sum_api.just_split([f.read()])
            
            files = glob.glob(f'{self.image_path}/*.png')
            files.sort(key=os.path.getmtime)
            print(files)
            
            for f in files:
                if os.path.isfile(f):
                    print(f)
                    self.image_list.append(f)
                    
            self.len = len(self.image_list)
            
            print('images num:', self.len)
            if self.len:
                self.image_prompt.setText(self.sum_list[self.image_idx])
                self.pre_img_btn.show()
                self.nxt_img_btn.show()
                self.im = QPixmap(self.image_list[self.image_idx])
                self.image_out.setPixmap(self.im)
            
    def next_image_event(self):
        if (self.image_idx) < (self.len-1):
            self.image_idx+=1 
            try:
                self.image_prompt.setText(self.sum_list[self.image_idx])
                self.im = QPixmap(self.image_list[self.image_idx])
                self.image_out.setPixmap(self.im)
                print(self.image_list[self.image_idx])
            except:
                pass
    
    def prv_image_event(self):
        if (self.image_idx) > 0:
            self.image_idx-=1 
            try:
                self.image_prompt.setText(self.sum_list[self.image_idx])
                self.im = QPixmap(self.image_list[self.image_idx])
                self.image_out.setPixmap(self.im)
                print(self.image_list[self.image_idx])
            except:
                pass

class display_story(QWidget):
    signal = pyqtSignal()
    go_back_signal = pyqtSignal()
    def __init__(self, parent = None):
        super(display_story,self).__init__(parent)
        self.tmp_parent = self.parent()
        self.signal.connect(self.parent().diff_gen_event)
        self.go_back_signal.connect(self.parent().go_back_event_story)
        self.space = QLabel()
        self.space2 = QLabel()
        self.prompt = None
        self.gpt_gen_btn = QPushButton('> Generate <')
        self.gpt_gen_btn.setFixedSize(300,60)
        self.gpt_gen_btn.clicked.connect(self.gpt_gen_event)
        
        self.show_btn = QPushButton('> show <')
        self.show_btn.setFixedSize(300,60)
        self.show_btn.clicked.connect(self.show_story_event)
        
        self.diff_gen_btn = QPushButton('> Generate Images <')
        self.diff_gen_btn.setFixedSize(300,60)
        self.diff_gen_btn.clicked.connect(lambda:self.signal.emit())
        
        self.back_btn = QPushButton('> go back <')
        self.back_btn.setFixedSize(300,60)
        self.back_btn.clicked.connect(lambda:self.go_back_signal.emit())
        self.story_out = QTextEdit()
        self.story_out.setFixedSize(600,300)
        self.story_out.setText('')
        f = QFont('Comic Sans MS',30)
        f2 = QFont('Comic Sans MS',16)
        
        self.story_out.setFont(f)
        self.diff_gen_btn.setFont(f2)
        self.gpt_gen_btn.setFont(f2)
        self.back_btn.setFont(f2)
        self.show_btn.setFont(f2)
        
        lay = QGridLayout()
        
        lay.addWidget(self.gpt_gen_btn,1,2)
        lay.addWidget(self.story_out,0,1)
        lay.addWidget(self.back_btn,3,0)
        lay.addWidget(self.show_btn,2,2)
        lay.addWidget(self.diff_gen_btn,3,2)
        self.setLayout(lay)
        self.pale = self.story_out.palette()
        self.pale.setColor(QPalette.Text, QColor("#8bddb0"))
        self.story_out.setPalette(self.pale)
        self.story_out.setStyleSheet(
                    "background-color: #EA6F57;\
                    color: #d4d4d4;\
                    font: bold 14px;"                   
                )
        self.diff_gen_btn.setStyleSheet(
                    "background-color:#0e1315;\
                    color: #15B5B0;\
                    font-weight: bold;"
                )
        self.back_btn.setStyleSheet(
                    "background-color:#0e1315;\
                    color: #15B5B0;\
                    font-weight: bold;"
                )
        self.show_btn.setStyleSheet(
                    "background-color:#0e1315;\
                    color: #15B5B0;\
                    font-weight: bold;"
                )
        self.gpt_gen_btn.setStyleSheet(
                    "background-color:#0e1315;\
                    color: #15B5B0;\
                    font-weight: bold;"
        )
        
    def show_story_event(self):
        if c.gpt_output != '':
            self.story_out.setText(c.gpt_output)
        else:
            self.story_out.setText('story not ready')
    
    def gpt_gen_event(self):
        print('gpt gen')
        print(f'prompt_from_client:{self.prompt}')
        if not c.GPT_out_state:
            c.s.sendall(f'GPT_GEN|{self.prompt}|{str(self.tmp_parent.get_slider_len())}'.encode())
    
class gpt_prompt(QWidget):
    signal = pyqtSignal()
    
    def __init__(self, parent = None):
        super(gpt_prompt,self).__init__(parent)
        self.signal.connect(self.parent().gpt_gen_btn_event)
        self.current_model_id = 'brathief/GPT_wwoo_10_5e-5'
        self.space = QLabel('    ')
        
        self.gpt_len_label = QLabel('story length:')
        self.gpt_len_slide_value = QLabel("150")
        
        self.gpt_len_slider = QSlider()
        self.gpt_len_slider.setOrientation(1)
        self.gpt_len_slider.setMaximum(1000)
        self.gpt_len_slider.setMinimum(50)
        self.gpt_len_slider.setValue(150)
        self.gpt_len_slider.valueChanged.connect(self.len_change_event)
        
        
        self.gpt_gen_btn = QPushButton('> Next <')
        self.gpt_gen_btn.setFixedSize(300,60)
        self.gpt_gen_btn.clicked.connect(lambda:self.signal.emit())
        
        self.init_gpt_req_btn = QPushButton('init\nGPT')
        self.init_gpt_req_btn.setFixedSize(60,60)
        self.init_gpt_req_btn.clicked.connect(self.init_gpt_req)
        
        self.gpt_check_label = QLabel()
        self.gpt_check_label.setFixedSize(20,20)
        
        self.prompt_box = QLineEdit()
        self.prompt_box.setText('dorothy lived in')
        self.prompt_box.setFixedSize(900,50)
        
        self.gpt_select_box = QComboBox()
        self.gpt_select_box.addItems([
            'brathief/GPT_wwoo_10_5e-5',
            'brathief/GPT_wwoo_10_5e-5|v2',
            'brathief/GPT_wwoo_10_5e-5|v3',
            
            'brathief/GPT_wwoo_20_5e-5',
            'brathief/GPT_wwoo_20_5e-5|v2',
            'brathief/GPT_wwoo_20_5e-5|v3',
            
            'brathief/GPT_alice_20_1e-5',
            'brathief/GPT_alice_20_1e-5|v2',
            'brathief/GPT_alice_20_2.5e-5',
            
            'brathief/GPT_nania_10_2.5e-5',
            'brathief/GPT_nania_20_5e-5',
            
            'brathief/GPT_grim_10_5e-5',
            'brathief/GPT_grim_30_5e-5',
            
            # 'brathief/GPT_Alice_417_e60',
            # 'brathief/GPT_alice_20',
            # 'brathief/GPT_grim_40',
            # 'brathief/GPT_nania_20',
            # 'brathief/GPT_wwoo_20',
            # 'brathief/GPT_blade_runner_20',
            # 'brathief/GPT_alice_wwoo_60',
        ])
        self.gpt_select_box.setFixedSize(300,40)
        self.gpt_select_box.setCurrentIndex(0)
        self.gpt_select_box.currentIndexChanged.connect(self.change_gpt_event)
        
        self.init_sum_req_btn = QPushButton('init\nSUM')
        self.init_sum_req_btn.setFixedSize(60,60)
        self.init_sum_req_btn.clicked.connect(self.init_sum_req)
        self.sum_check_label = QLabel()
        self.sum_check_label.setFixedSize(20,20)
        
        self.init_translate_btn = QPushButton('activate Translation mode')
        self.init_translate_btn.setFixedSize(300,40)
        self.init_translate_btn.clicked.connect(self.init_tran_req)
        
        self.tran_check_label = QLabel()
        self.tran_check_label.setFixedSize(20,20)
        self.tran_state = False
        self.lock = 0
       
        
        f1 = QFont('Comic Sans MS',14)
        f2 = QFont('Comic Sans MS', 12)
        self.prompt_box.setFont(f1)
        self.gpt_gen_btn.setFont(f2)
        self.init_gpt_req_btn.setFont(f2)
        self.init_sum_req_btn.setFont(f2)
        self.gpt_select_box.setFont(f2)
        self.gpt_len_slide_value.setFont(f2)
        self.gpt_len_label.setFont(f2)
        self.init_translate_btn.setFont(f2)
        
        lay = QGridLayout()
        lay.addWidget(self.gpt_len_slider,1,2)
        
        lay.addWidget(self.gpt_len_label,1,3)
        lay.addWidget(self.gpt_len_slide_value,1,4)
        lay.addWidget(self.space,4,0)
        lay.addWidget(self.space,5,0)
        
        lay.addWidget(self.space,1,4)
        lay.addWidget(self.space,2,4)
        lay.addWidget(self.space,3,4)
        lay.addWidget(self.space,4,4)
        lay.addWidget(self.space,5,4)
        lay.addWidget(self.init_translate_btn,0,5)
        lay.addWidget(self.tran_check_label,0,6)
        lay.addWidget(self.gpt_select_box,0,0)
        lay.addWidget(self.init_gpt_req_btn,0,1)
        lay.addWidget(self.gpt_check_label,0,2)
        
        #lay.addWidget(self.init_sum_req_btn,0,3)
        #lay.addWidget(self.sum_check_label,0,4)
        #lay.addWidget(self.space,0,5)
        
        lay.addWidget(self.prompt_box,5,2,1,3)
        lay.addWidget(self.gpt_gen_btn,5,10)
        
        self.setLayout(lay)
        
        p_pale = self.prompt_box.palette()
        p_pale.setColor(QPalette.Text, QColor("#dd8bb8"))
        self.prompt_box.setPalette(p_pale)
        self.prompt_box.setStyleSheet(
                    "background-color:#3B3B3B;\
                    color: #E08955;\
                    border-style: outset;\
                    border-width: 2px;\
                    border-radius: 10px;\
                    border-color: #9B9B9B;\
                    font: bold 14px;\
                    min-width: 10em;\
                    padding: 10px;"
        )
        self.tran_check_label.setStyleSheet(
                    "background-color:#9c142d;\
                    color: #15B5B0;\
                    border: solid;\
                    border-radius: 2px;\
                    border-width: 2px;\
                    border-radius: 10px;\
                    border-color: #161418;"          
        ) if self.tran_state != True else self.tran_check_label.setStyleSheet(
                    "background-color:#1ff177;\
                    color: #15B5B0;\
                    border: solid;\
                    border-radius: 2px;\
                    border-width: 2px;\
                    border-radius: 10px;\
                    border-color: #161418;"
        )
        self.init_translate_btn.setStyleSheet(
                    "background-color:#6F57EA;\
                    color: #F15152;\
                    font-weight: bold;"
        )
        self.gpt_select_box.setStyleSheet(
                    "background-color:#0e1315;\
                        color: #15B5B0;"
        )
        self.gpt_gen_btn.setStyleSheet(
                    "background-color:#0e1315;\
                    color: #15B5B0;\
                    font-weight: bold;"
        )
        self.init_gpt_req_btn.setStyleSheet(
                    "background-color:#6F57EA;\
                    color: #F15152;\
                    font-weight: bold;"
        )
        self.init_sum_req_btn.setStyleSheet(
                    "background-color:#F9BDC0;\
                    color: #ff4500;\
                    font-weight: bold;"
        )
        self.gpt_check_label.setStyleSheet(
                    "background-color:#9c142d;\
                    color: #15B5B0;\
                    border: solid;\
                    border-radius: 2px;\
                    border-width: 2px;\
                    border-radius: 10px;\
                    border-color: #161418;"
                    
        ) if c.GPT_state != True else self.gpt_check_label.setStyleSheet(
                    "background-color:#1ff177;\
                    color: #15B5B0;\
                    border: solid;\
                    border-radius: 2px;\
                    border-width: 2px;\
                    border-radius: 10px;\
                    border-color: #161418;"
        )
        self.sum_check_label.setStyleSheet(
                    "background-color:#9c142d;\
                    color: #15B5B0;\
                    border: solid;\
                    border-radius: 2px;\
                    border-width: 2px;\
                    border-radius: 10px;\
                    border-color: #161418;"
                    
        ) if c.sum_state != True else self.sum_check_label.setStyleSheet(
                    "background-color:#1ff177;\
                    color: #15B5B0;"
        )
        self.gpt_len_slider.setStyleSheet(
            '''
            QSlider {
                border-radius: 10px;
            }
            QSlider::groove:horizontal {
                height: 5px;
                background: #000;
            }
            QSlider::handle:horizontal{
                background: #f00;
                width: 16px;
                height: 16px;
                margin:-6px 0;
                border-radius:8px;
            }
            QSlider::sub-page:horizontal{
                background:#f90;
            }
            '''
        )
        self.gpt_len_slide_value.setStyleSheet(
            "color: #FF3660;"
        )
        self.gpt_len_label.setStyleSheet(
            "color: #bcbcbc;"
        )
        
    def init_tran_req(self):
        if c.tran_state == True:
            self.tran_check_label.setStyleSheet(
                    "background-color:#1ff177;\
                    color: #15B5B0;\
                    border: solid;\
                    border-radius: 2px;\
                    border-width: 2px;\
                    border-radius: 10px;\
                    border-color: #161418;"
            )
        else:
            print('req for tran model')
            c.s.send('TRAN_INIT|'.encode())
                
    def init_gpt_req(self):          
        
        if c.GPT_state == True:
            self.gpt_check_label.setStyleSheet(
                    "background-color:#1ff177;\
                    color: #15B5B0;\
                    border: solid;\
                    border-radius: 2px;\
                    border-width: 2px;\
                    border-radius: 10px;\
                    border-color: #161418;"
        )
        else:
            model_parse = self.current_model_id.split('|')
            try:
                gpt_version = model_parse[1]
                print(f'{gpt_version} model')
            except IndexError:
                gpt_version = 'v1'
                print('v1 model')
            print(f'req for {self.current_model_id} gpt model...')
            c.s.send(f'GPT_INIT|{model_parse[0]}|{gpt_version}'.encode())
            
    def init_sum_req(self):
        if c.sum_state == True:
            self.sum_check_label.setStyleSheet(
                    "background-color:#1ff177;\
                    color: #15B5B0;\
                    border: solid;\
                    border-radius: 2px;\
                    border-width: 2px;\
                    border-radius: 10px;\
                    border-color: #161418;"
        )
        else:
            c.s.send(f'SUM_INIT|'.encode())
    
    def change_gpt_event(self):
        self.gpt_check_label.setStyleSheet(
                    "background-color:#9c142d;\
                    color: #15B5B0;\
                    border: solid;\
                    border-radius: 2px;\
                    border-width: 2px;\
                    border-radius: 10px;\
                    border-color: #161418;"
        )
        self.current_model_id = self.gpt_select_box.currentText()
        c.s.send('GPT_CLEAR|'.encode())
        c.GPT_state = False
        print('current gpt model id: ',self.current_model_id)
        
    def len_change_event(self):
        self.gpt_len_slide_value.setText(str(self.gpt_len_slider.value()))
    # def init_diff_req(self):
    #     if c.diff_state == True:
    #         self.diff_model_check_label.setStyleSheet(
    #                 "background-color:#1ff177;\
    #                 color: #15B5B0;"
    #     )
    #     else:
    #         c.s.send(f'DIFF_INIT-'.encode())
class little_share_win(QWidget):
    next_signal = pyqtSignal()
    prv_signal = pyqtSignal()
    go_back_signal = pyqtSignal()
    def __init__(self):
        super(little_share_win,self).__init__()
        
        self.win_p = self.palette()
        self.win_p.setColor(QPalette.Window, QColor("#161418"))
        self.setPalette(self.win_p)
        self.setGeometry(100,100,600,600)
        
        self.next_signal.connect(self.next_image_event)
        self.prv_signal.connect(self.prv_image_event)
        self.pre_img_btn = QPushButton('Previous')
        self.nxt_img_btn = QPushButton('Next')
        self.show_btn = QPushButton('show')
        self.tra = QPushButton('show')
        self.space = QLabel('    ')
        
        self.image_prompt = QTextEdit()
        self.image_prompt.setFixedSize(512,200)
        
        self.translate_btn = QPushButton('Translate')
        self.translate_btn.setFixedSize(150,30)
        self.translate_btn.clicked.connect(self.tran_translate)
        
        self.tran_show_btn = QPushButton('show tran')
        self.tran_show_btn.setFixedSize(150,30)
        self.tran_show_btn.clicked.connect(self.tran_show)
        
        self.image_out = QLabel()
        self.image_out.setFixedSize(512,512)
        self.nxt_img_btn.clicked.connect(lambda:self.next_signal.emit())
        self.pre_img_btn.clicked.connect(lambda:self.prv_signal.emit())
        self.nxt_img_btn.hide()
        self.pre_img_btn.hide()
        self.show_btn.clicked.connect(self.show_event)
        self.image_list = []
        self.sum_list = []
        self.image_path = 'client_tmp'
        self.image_idx = 0
        
        f = QFont('Comic Sans MS',30)
        f2 = QFont('Comic Sans MS',16)
        
        self.image_prompt.setFont(f)
        self.show_btn.setFont(f2)
        self.pre_img_btn.setFont(f2)
        self.image_out.setFont(f2)
        self.nxt_img_btn.setFont(f2)
        self.image_prompt.setFont(f2)
        self.tran_show_btn.setFont(f2)
        self.translate_btn.setFont(f2)
        lay = QGridLayout()
        lay.addWidget(self.show_btn,1,3)
        lay.addWidget(self.pre_img_btn,2,2)
        lay.addWidget(self.image_out,2,3)
        lay.addWidget(self.nxt_img_btn,2,4)
        lay.addWidget(self.image_prompt,2,1)
        lay.addWidget(self.translate_btn,3,1)
        lay.addWidget(self.tran_show_btn,4,1)
        self.setLayout(lay)
        
        self.tran_show_btn.setStyleSheet(
                    "background-color:#0e1315;\
                    color: #15B5B0;\
                    font-weight: bold;"
        )
        
        self.translate_btn.setStyleSheet(
                    "background-color:#0e1315;\
                    color: #15B5B0;\
                    font-weight: bold;"
        )
        
        self.nxt_img_btn.setStyleSheet(
                    "background-color:#252033;\
                    color: #15B5B0;\
                    border-style: outset;\
                    border-width: 2px;\
                    border-radius: 10px;\
                    border-color: #F9BDC0;\
                    font: bold 14px;\
                    min-width: 10em;\
                    padding: 10px;"
        ) 
        self.pre_img_btn.setStyleSheet(
                    "background-color:#252033;\
                    color: #15B5B0;\
                    border-style: outset;\
                    border-width: 2px;\
                    border-radius: 10px;\
                    border-color: #F9BDC0;\
                    font: bold 14px;\
                    min-width: 10em;\
                    padding: 10px;"
        )
        self.show_btn.setStyleSheet(
                    "background-color:#252033;\
                    color: #15B5B0;\
                    border-style: outset;\
                    border-width: 2px;\
                    border-radius: 10px;\
                    border-color: #F9BDC0;\
                    font: bold 14px;\
                    min-width: 10em;\
                    padding: 10px;"
        ) 
        self.image_prompt.setStyleSheet(
            "background-color: #8F3D50;\
            color: #d4d4d4;\
            font: bold 14px;"                   
        )
    

    def tran_show(self):
        if c.galary_translated_text:
            self.image_prompt.setText(f'{self.sum_list[self.image_idx]}\n{c.galary_translated_text}')

    def tran_translate(self):
        if len(self.sum_list) > 0 and c.tran_state:
            print('req for galary translate')
            c.s.send(f'TRAN_GALARY_GEN|{self.sum_list[self.image_idx]}|{self.image_idx}'.encode())

    def show_event(self):
        if c.galary_state:
            self.init_images()

    def init_images(self):
        self.image_idx = 0
        self.image_list = []
        self.sum_list = sum_api.just_split([c.galary_story])

        files = glob.glob(f'{self.image_path}/*.png')
        #files.sort(key=os.path.getmtime)
        files = sorted(files, key=lambda y: (y.rsplit('.', 1)[0]))
        print('files in galary:',files)

        for f in files:
            if os.path.isfile(f):
                print(f)
                self.image_list.append(f)

        self.len = len(self.image_list)
        if self.len:
            self.im = QPixmap(self.image_list[self.image_idx])
            self.image_out.setPixmap(self.im)
            self.pre_img_btn.show()
            self.nxt_img_btn.show()
            self.image_prompt.setText(self.sum_list[0])

        for i in self.image_list:
            print(i)

    def next_image_event(self):
        if self.len:
            if (self.image_idx) < (self.len-1):
                print('self.image_idx:',self.image_idx)
                self.image_idx+=1 
                try:
                    self.image_prompt.setText(self.sum_list[self.image_idx])
                    self.im = QPixmap(self.image_list[self.image_idx])
                    self.image_out.setPixmap(self.im)
                    print(self.image_list[self.image_idx])
                except:
                    pass

    def prv_image_event(self):
        if self.len:
            if (self.image_idx) > 0:
                print('self.image_idx:',self.image_idx)
                self.image_idx-=1 
                try:
                    self.image_prompt.setText(self.sum_list[self.image_idx])
                    self.im = QPixmap(self.image_list[self.image_idx])
                    self.image_out.setPixmap(self.im)
                    print(self.image_list[self.image_idx])
                except:
                    pass

class Share_win(QWidget):
    def __init__(self):
        super(Share_win,self).__init__()
        self.galary_show_btn = QPushButton('== look up galary ==')
        self.galary_show_btn.setFixedSize(100,50)
        self.galary_show_btn.clicked.connect(self.show_galary_event)
        self.path = 'client_tmp'
        self.win_p = self.palette()
        self.win_p.setColor(QPalette.Window, QColor("#161418"))
        self.setPalette(self.win_p)
        self.setGeometry(100,100,600,600)
        
        
        self.scroll = QScrollArea()
        
        self.layout = QVBoxLayout()
        self.layout.addWidget(self.galary_show_btn)
        self.layout.addWidget(self.scroll)
        
        self.setLayout(self.layout)
        
        self.galary_req_event()
        
        self.galary_show_btn.setStyleSheet(
                    "background-color:#252033;\
                    color: #15B5B0;\
                    border-style: outset;\
                    border-width: 2px;\
                    border-radius: 10px;\
                    border-color: #F9BDC0;\
                    font: bold 14px;\
                    min-width: 10em;\
                    padding: 10px;"
        )
        
    def show_galary_event(self):
        self.layout.removeWidget(self.scroll)
        self.formLayout = QFormLayout()
        self.groupBox = QGroupBox()
        
        for filename in c.galary_name_list:
            print('button name:',filename)
            btn = QPushButton(filename)
            btn.setObjectName(filename)
            btn.installEventFilter(self)
            
            btn.clicked.connect(lambda:self.galary_story_req_event())
            btn.setStyleSheet(
                "background-color:#252033;\
                    color: #15B5B0;\
                    border-style: outset;\
                    border-width: 2px;\
                    border-radius: 10px;\
                    border-color: #F9BDC0;\
                    font: bold 14px;\
                    min-width: 10em;\
                    padding: 10px;"
            )
            self.formLayout.addRow(btn)
        
        self.groupBox.setLayout(self.formLayout)
        self.scroll = QScrollArea()
        self.scroll.setWidgetResizable(True)
        self.scroll.setWidget(self.groupBox)
        
        self.layout.addWidget(self.scroll)
        
        
    def galary_req_event(self):        
        print('req for galary list')
        c.s.send('GALARY_INIT|'.encode())
        
    def galary_story_req_event(self,filename='test'):
        c.tmp_num = 0
        files = glob.glob(f'{self.path}/*')
        for f in files:
            os.remove(f)
        print(f'starting req for {self.sender().objectName()}')
        c.s.setblocking(1)
        c.s.send(f'GALARY_REQ|{self.sender().objectName()}'.encode())
        
        self.little_share_win = little_share_win()
        self.little_share_win.show()
    # def eventFilter(self, obj, event):
    #     if event.type() == QEvent.MouseButtonRelease:
    #         print(f'obj:{obj.objectName()}')
        
    #     return super().eventFilter(obj, event)
        
        

def create_win():
    app = QApplication(sys.argv)
    app.setStyle('Fusion')
    win = MainWin()
    win.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    #user_name = input('input your user_name')
    user_name = 'user1'
    c = client.client(user_name)
    socket_th_client = QThread()
    socket_th_client.run = c.send_info
    socket_th_client.start()
    
    create_win()
