from transformers import AutoTokenizer, AutoModelForSeq2SeqLM ,AutoModelForCausalLM
import torch
from diffusers import StableDiffusionPipeline
import requests
import json
import PIL
import io
from PIL import Image

API_URL = "https://api-inference.huggingface.co/models/brathief/GPT_Alice_417_e60"
headers = {"Authorization": "Bearer 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"}

class gpt_config():
    def __init__(self, model_id = None, version = None):
        assert model_id != None, 'you idiot!  gpt can not be none'
        self.model_id = model_id
        print('configing , model_id is :', self.model_id)
        if version == 'v1':
            with open(f'api\gpt_config\{self.model_id}.json') as f:
                try:
                    print('loading v1 gpt config!')
                    self.config = json.load(f)
                except:
                    print('error')
        else:
            with open(f'api\gpt_config\{self.model_id}_{version}.json') as f:
                try:
                    print('loading gpt config!')
                    self.config = json.load(f)
                except:
                    print('error')
        
        self.max_length = 550
        self.min_length = 20
        self.top_k = None
        self.top_p = None
        self.temperature = None
        self.repetition_penalty = None
        self.do_sample = None
        self.num_return_sequences = None
        self.init_param()
        
    def init_param(self):
        try:
            self.max_length = self.config['max_length'] if self.config['max_length'] else None
        except:
            pass
        try:
            self.min_length = self.config['min_length'] if self.config['min_length'] else None
        except:
            pass
        try:
            self.top_k = self.config['top_k'] if self.config['top_k'] else None
        except:
            pass
        try:
            self.top_p = self.config['top_p'] if self.config['top_p'] else None
        except:
            pass
        try:
            self.temperature = self.config['temperature'] if self.config['temperature'] else None
        except:
            pass
        try:
            self.repetition_penalty = self.config['repetition_penalty'] if self.config['repetition_penalty'] else None
        except:
            pass
        try:
            self.do_sample = self.config['do_sample'] if self.config['do_sample'] else None
        except:
            pass
        try:
            self.num_return_sequences = self.config['num_return_sequences'] if self.config['num_return_sequences'] else None
        except:
            pass
        try:
            self.num_beams = self.config['num_beams'] if self.config['num_beams'] else None
        except:
            pass
        try:
            self.num_beam_groups = self.config['num_beam_groups'] if self.config['num_beam_groups'] else None
        except:
            pass
        try:
            self.no_repeat_ngram_size = self.config['no_repeat_ngram_size'] if self.config['no_repeat_ngram_size'] else None
        except:
            pass
        try:
            self.diversity_penalty = self.config['diversity_penalty'] if self.config['diversity_penalty'] else None
        except:
            pass
        try:
            self.early_stopping = self.config['early_stopping'] if self.config['early_stopping'] else None
        except:
            pass
        

# def gpt_query(payload):
#     data = json.dumps(payload)
#     response = requests.request("POST", API_URL, headers=headers, data=data)
#     return json.loads(response.content.decode("utf-8"))

# def gpt_api_gen(prompt):
#     print('api requesting...')
#     data = gpt_query(
#             {
#                 'inputs': f'{prompt}',
#                 'parameters':{
#                     'top_k':50,
#                     'top_p':0.92,
#                     'do_sample':True,
#                     'max_length': 100,
#                     'min_length': 20,
#                     'temperature': 0.7,
#                     'num_return_sequences': 1,
#                 },
#                 'options':{
#                     'use_cache': False,
#                 }
#             }
#         )
#     return data

def gpt_loader(model_id = 'brathief/GPT_Alice_417_e60'):
    
    tokenizer = AutoTokenizer.from_pretrained(model_id)
    model = AutoModelForCausalLM.from_pretrained(model_id)
    
    print('hi')
    device = torch.device('cuda')
    model = model.to(device)
    model.eval()
    print('hi_2')
    print(f'model_id:{model_id} created')
    
    return model, tokenizer, 0

def gpt2_gen(model,
            tokenizer,
            model_id,
            prompt = 'Alice',
            length = None,
            version = None
    ):
    
    device = 'cuda'
    input_ids = torch.tensor(tokenizer.encode(prompt)).unsqueeze(0).to(device)
    gpt_param = gpt_config(model_id=model_id, version = version)
    print(f'GPT-configs: {vars(gpt_param)}')
    
    
    if gpt_param.do_sample == True:
        print('====do sample====')
        print('length:',length)
        
        generated = model.generate(
            input_ids,
            max_length = length,
            min_length = length, 
            num_return_sequences = gpt_param.num_return_sequences,
            top_k = gpt_param.top_k, 
            top_p = gpt_param.top_p,
            temperature = gpt_param.temperature,
            do_sample = True,
            repetition_penalty = gpt_param.repetition_penalty,      
        ).to(device)

        text_from_gpt = []
        for i, text in enumerate(generated):
            print(tokenizer.decode(text, skip_special_tokens = True))
            text_from_gpt.append(tokenizer.decode(text, skip_special_tokens = True))

        return text_from_gpt

    ###====beam====
    else:
        print('====beam mode====')
        print('length:',length)
        generated = model.generate(
            input_ids,
            max_length = length,
            min_length = length, 
            num_return_sequences = gpt_param.num_return_sequences,
            num_beams = gpt_param.num_beams, 
            no_repeat_ngram_size = gpt_param.no_repeat_ngram_size,
            num_beam_groups = gpt_param.num_beam_groups,
            diversity_penalty = gpt_param.diversity_penalty,
        ).to(device)

        text_from_gpt = []
        for i, text in enumerate(generated):
            print(tokenizer.decode(text, skip_special_tokens = True))
            text_from_gpt.append(tokenizer.decode(text, skip_special_tokens = True))
            
        return text_from_gpt
    
def release():
    torch.cuda.empty_cache()