from transformers import AutoTokenizer, AutoModelForSeq2SeqLM ,AutoModelForCausalLM
import torch
from diffusers import StableDiffusionPipeline
import requests
import json
import PIL
import io
from PIL import Image
import math
API_URL = "https://api-inference.huggingface.co/models/knkarthick/MEETING_SUMMARY"
headers = {"Authorization": "Bearer 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"}

def query(payload):
	response = requests.post(API_URL, headers=headers, json=payload)
	return response.json()
def sum_api_gen(text_from_gpt):
    
    output = query({
        "inputs": "The tower is 324 metres (1,063 ft) tall, about the same height as an 81-storey building, and the tallest structure in Paris. Its base is square, measuring 125 metres (410 ft) on each side. During its construction, the Eiffel Tower surpassed the Washington Monument to become the tallest man-made structure in the world, a title it held for 41 years until the Chrysler Building in New York City was finished in 1930. It was the first structure to reach a height of 300 metres. Due to the addition of a broadcasting aerial at the top of the tower in 1957, it is now taller than the Chrysler Building by 5.2 metres (17 ft). Excluding transmitters, the Eiffel Tower is the second tallest free-standing structure in France after the Millau Viaduct.",
    })

def sum_loader(model_id = 'knkarthick/MEETING_SUMMARY'):
    sum_tokenizer = AutoTokenizer.from_pretrained(model_id)
    sum_model = AutoModelForSeq2SeqLM.from_pretrained(model_id)
    print(f'sum_model:{model_id} created')
    return sum_model, sum_tokenizer , 0

def normal_split(text_from_gpt, sum_model, sum_tokenizer):
    normal_list = []
    for text in text_from_gpt:
        normal_list.extend([x + '.' for x in text.split('.') if x])
    
def summarize_period(text_from_gpt, sum_model, sum_tokenizer, sum_dict = {}):
    assert sum_dict != {}, 'sum_dict can not be null'
    print('using summarize_period mode')
    sum_model.eval()
    sum_model = sum_model.to('cuda')
    sum_list = []
    for text, sum_min in zip(text_from_gpt, sum_dict.values()):
        input_ids = sum_tokenizer.encode(text, return_tensors="pt").to('cuda')
        print(f'miniest summary gen len:{sum_min}')
        generated_sequence = sum_model.generate(
            input_ids = input_ids,
            #no_repeat_ngram_size = 3,
            #num_beams = 20,
            #num_beam_groups = 2,
            max_length = (int(sum_min) + 5),
            min_length = int(sum_min),
            #temperature = 1.0,
            #top_k = 20,
            #top_p = 0.95,
            #do_sample = True,
        ).to('cuda')
        output_text = sum_tokenizer.decode(generated_sequence.squeeze(), skip_special_tokens=True)
        print(output_text)
        sum_list.extend( [x+',' for x in output_text.split('.') if x] )

    return sum_list

def sum_short_iter(text_from_sum, sum_model, sum_tokenizer, sum_short_min = 30):
    #assert sum_short_min > 30,'sum_short_min has to be > 30'
    print('using sum_short_iter mode')
    sum_model.eval()
    sum_model = sum_model.to('cuda')
    last_sum = ''
    final_sum = []
    delimiter = '.'
    for current_s in text_from_sum:   
        need_check_str = last_sum + current_s
        
        need_check_str_len = len(need_check_str.split())
        
        if need_check_str_len > 30 :
            
            input_ids = sum_tokenizer.encode(need_check_str, return_tensors="pt").to('cuda')
            generated_sequence = sum_model.generate(
                input_ids = input_ids,
                #no_repeat_ngram_size = 3,
                #num_beams = 20,
                #num_beam_groups = 2,
                max_length = sum_short_min, #this is not the true length of output words
                min_length = sum_short_min,
                #temperature = 1.0,
                #top_k = 20,
                #top_p = 0.95,
                #do_sample = True,
            ).to('cuda')
            output_text = sum_tokenizer.decode(generated_sequence.squeeze(), skip_special_tokens=True)
            
            final_sum.append(output_text)

            def cut_it(output_text):
                for i in range(1,100):
                    if output_text.split('.')[-i]:
                        cut_the_sum = output_text.split(',')[-i]
                        

                        if cut_the_sum[-1] != ',' and cut_the_sum[-1] != '.':
                            cut_the_sum += ','

                        return cut_the_sum
            

            last_sum = ''
            #print('last_sum:',last_sum)
        else:
            #final_sum.append(need_check_str)
            last_sum = need_check_str
    return final_sum

def summarize(text_from_gpt, sum_model, sum_tokenizer, use_sum_short_iter = False, sum_mole = 3, sum_deno = 2):
    
    sum_dict = {}
    i = 1
    for t in text_from_gpt:
        words = len(t.split())
        print('words:',words)
        sum_factor = math.ceil(words / sum_mole * 1) / 1.0 * sum_deno    
        sum_dict.update({i: sum_factor})
        i += 1
    
    sum_list = summarize_period(text_from_gpt, sum_model, sum_tokenizer, sum_dict = {})
    
    if use_sum_short_iter:
        sum_list = sum_short_iter(sum_list, sum_model, sum_tokenizer, sum_short_min = 30)
        return sum_list
    
    return sum_list

def just_split(text_from_gpt):
    sum_l = []
    for text in text_from_gpt:
        print(text)
        for x in text.split('.'):
            sum_l.append(x)
    
    return sum_l

