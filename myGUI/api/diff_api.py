from transformers import AutoTokenizer, AutoModelForSeq2SeqLM ,AutoModelForCausalLM
import torch
from diffusers import StableDiffusionPipeline, DPMSolverMultistepScheduler
import requests
import json
import PIL
import io
from PIL import Image

API_TOKEN = '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'
# API_URL = "https://api-inference.huggingface.co/models/brathief/Alice_extend_brathief_e500"
# headers = {"Authorization": "Bearer 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"}
API_URL = "https://api-inference.huggingface.co/models"
headers = {"Authorization": f"Bearer {API_TOKEN}"}

def query(model_id, payload):
    print(f'diffusion api requesting: {API_URL}/{model_id}')
    response = requests.post(f'{API_URL}/{model_id}', headers=headers, json=payload)
    print(response)
    return response.content

def diff_api_gen(sum_list, client_sock, client_name, special_prompt, model_id, num_process, guidance_scale):
    print('num_process',int(num_process))
    
    print('guidance_scale',float(guidance_scale))
    
    client_sock.setblocking(1)
    print(type(client_sock))
    i = 0
    
    for sum in sum_list:
        print('image prompt:', sum)
        print(f'full prompt: {sum},{special_prompt}')
        image_bytes = query(model_id,{
            "inputs": f"{sum},{special_prompt}",
            'parameters':{
                'num_inference_steps' : int(num_process),
                'guidance_scale' : float(guidance_scale),
            },
            'options': {
                'use_cache': False,
                'wait_for_model': True,
            }
        })
        try:
            print('sending images')
            temp = str(i).rjust(3,'0')
            image = Image.open(io.BytesIO(image_bytes))
            image.save(f'./users/{client_name}/diff_out_{temp}.png')
            image_r =  open(f'./users/{client_name}/diff_out_{temp}.png', "rb")
            client_sock.send('DIFF_OUT|'.encode())
            while True:
                imgData = image_r.readline(4096)
                client_sock.send(imgData)
                if not imgData:
                    print('data clear')
                    break  # 讀完檔案結束迴圈
            image_r.close()
            #image.save(f'out/diff_out_{i}.png')
            i+=1
        except PIL.UnidentifiedImageError:
            return 0
    client_sock.setblocking(0)
    return 1

def diff_loader(lora_model_path = './lora_weights/Alice-lora.bin',pretrained = True, pretrained_id = 'runwayml/stable-diffusion-v1-5'):
    if pretrained:
        print('using pretrained diffusion')
        pipe = StableDiffusionPipeline.from_pretrained(pretrained_id, device_map = None, torch_dtype=torch.float16).to('cuda')
        pipe = pipe.to("cuda")
        pipe = pipe.to("cuda")
        pipe = pipe.to("cuda")
        return pipe, 0
    else:
        print('using trained diffusion')
        pipe = StableDiffusionPipeline.from_pretrained(pretrained_id, torch_dtype=torch.float16).to('cuda')
        pipe.scheduler = DPMSolverMultistepScheduler.from_config(pipe.scheduler.config)
        pipe.unet.load_attn_procs(lora_model_path)
        return pipe, 0
    
def diff_gen(
        prompt_from_gpt,
        pipe,
        num_inference_steps = 50,
        guidance_scale = 7.5,
        multi_img = False,
        cross = False,
        cross_value = 0.5,
        num_images = 1,
    ):
    if not multi_img:
        i = 1
        for prompt in prompt_from_gpt:
            image = pipe(
                prompt,
                num_inference_steps = num_inference_steps,
                guidance_scale = guidance_scale,
            ).images[0]  

            image.save(f"/content/drive/MyDrive/vscode_python/gpt&diffusion/output/single/output_{i}.png")
            print(f'image saved in output_{i}.png')
            i+=1
            
    elif cross:
        for prompt in prompt_from_gpt:
            image = pipe(
                prompt, num_inference_steps=num_inference_steps, guidance_scale=guidance_scale, cross_attention_kwargs={"scale": cross_value}
            ).images[0]
            image.save(f"/content/drive/MyDrive/vscode_python/gpt&diffusion/output/single/output_{i}.png")
            i+=1
            
    elif multi_img:
        p_idx = 1
        for prompt in prompt_from_gpt:
            prompt_L = []
            prompt_L.append(prompt)
            prompt_L = prompt_L * num_images
            images = pipe(
                prompt_L,
                num_inference_steps = num_inference_steps,
                guidance_scale = guidance_scale,
            ).images

            p_idx += 1
            for idx in range(0, len(images)):
                images[idx].save(f"/content/drive/MyDrive/vscode_python/gpt&diffusion/output/multi/prompt_{p_idx}_output_{idx}.png")
                
    
