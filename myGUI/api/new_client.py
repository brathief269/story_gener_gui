import glob
import shutil
import socket
from threading import *
from _thread import *
import threading
import tran_api
import PIL
import gpt_api
import sum_api
import diff_api
import galary_api
import time, os
from PIL import Image

class client(Thread):
    def __init__(self, socket, address):
        Thread.__init__(self)
        self.client_sock = socket
        self.alive = True
        self.client_addr = address
        self.client_name = ''
        print(self.client_sock)
        print(self.client_sock.fileno())
        self.client_sock.setblocking(True)
        
        self.gpt_version = None
        self.galary_sum_list = []
        self.gpt_model = None
        self.tran_model = None
        self.gpt_model_id = ' '
        self.diff_model_id = ' '
        self.gpt_lock = 0
        self.sum_lock = 0
        self.diff_lock = 0
        self.tran_lock = 0
        self.gpt_state = 0
        self.sum_state = 0
        self.diff_state = 0
        self.tran_state = False
        self.galary_name_list = []
        
        self.gpt_output = []
        self.gpt_output.append('Alice sit by the river.Alice sit by the river with a fox')
        self.daemon = True
        self.t_s = Thread(target=self.t_send,daemon=True)
        self.t_r = Thread(target=self.t_recv,daemon=True)
        
        self.sum_list = []
        #self.watch_dog = Thread(target=self.check_alive,daemon=True)
        
        self.getclient_info()
        
    def getclient_info(self):
        print('recving info...')
        while True:
            try:
                self.client_name = self.client_sock.recv(1024).decode()
                print(f'client_name: {self.client_name}')
                print('checking user data...')
                dir = os.path.isdir(f'./users/{self.client_name}')
                if not dir:
                    print('=== user loggin first time!! welcome ===')
                    os.mkdir(f'./users/{self.client_name}')
                    print('data created')
                break
            except:
                print('waiting for client info ')
        
        self.client_sock.setblocking(False)
        self.t_r.start()
        self.t_s.start()
        #self.watch_dog.start()
        
    def t_send(self):
        print('send thread')
        while True and self.alive == True:
            try:
                send = self.client_sock.send(input().encode())
                print('server sent:', send)
            except:
                pass
        
    def t_recv(self):
        print('recv thread')
        while True and self.alive == True:
            # print('holding...')
            #print('sum_list:',self.sum_list)
            try:
                recv = self.client_sock.recv(99999).decode()
                print('server recv:',recv)
                recv_sparse = recv.split('|')
                if recv_sparse[0] == 'GPT_INIT':
                    
                    model_id = recv_sparse[1]
                    self.gpt_version = recv_sparse[2]
                    print('gpt_version: ',self.gpt_version)
                    a = model_id.split('/')[1]
                    self.gpt_model_id = a
                    
                    print('gpt model_id:',model_id)
                    print('self.gptmodel:',self.gpt_model_id)
                    if self.gpt_lock == 0 and self.gpt_state == 0:
                        print('server initing gpt')
                        self.gpt_lock = 1
                        self.gpt_model, self.gpt_tokenizer, self.gpt_lock = gpt_api.gpt_loader(
                            model_id=model_id, 
                            
                        )
                        self.gpt_state = 1
                        print('GPT_CREATED')
                        self.client_sock.send('GPT_CREATED|'.encode())
                
                elif recv_sparse[0] == 'GPT_CLEAR':
                    if self.gpt_state and self.gpt_model:
                        del self.gpt_model
                        gpt_api.release()
                        
                    self.gpt_state = 0
                    
                elif recv_sparse[0] == 'GPT_GEN':
                    print("gpt specify len:",recv_sparse[2])
                    if self.gpt_state:
                        print('server gening gpt')
                        prompt = recv_sparse[1]
                        print('client prompt:', prompt)
                        text_from_gpt = gpt_api.gpt2_gen(
                            model=self.gpt_model, 
                            tokenizer=self.gpt_tokenizer, 
                            model_id = self.gpt_model_id, 
                            prompt=prompt, 
                            length = int(recv_sparse[2]),
                            version = self.gpt_version,
                        )
                        
                        self.gpt_output[0] = text_from_gpt[0]
                        
                        with open(f'./users/{self.client_name}/story.txt','w',encoding='utf-8') as f:
                            print('writing story.txt')
                            f.write(self.gpt_output[0])
                            
                        self.client_sock.send(f'GPT_GEN_OK|{self.gpt_output[0]}'.encode())
                        print('GPT_GEN_OK')
                elif recv_sparse[0] == 'SUM_INIT':
                    
                    if self.sum_lock == 0 and self.sum_state == 0:
                        print('server initing sum')
                        self.sum_lock = 1
                        self.sum_model, self.sum_tokenizer, self.sum_lock = sum_api.sum_loader() 
                        self.sum_state = 1
                        # print('server gening sum')
                        # self.sum_list = sum_api.summarize(
                        #     text_from_gpt=self.gpt_output,
                        #     sum_model=self.sum_model,
                        #     sum_tokenizer = self.sum_tokenizer
                        # )
                        # print('SUM_CREATED')    
                        self.client_sock.send('SUM_CREATED|'.encode())
                        
                elif recv_sparse[0] == 'SUM_GEN':
                    
                    if self.sum_state:
                        print('gpt out:',self.gpt_output)
                        self.sum_list = sum_api.summarize(
                            text_from_gpt=self.gpt_output, 
                            sum_model=self.sum_model,
                            sum_tokenizer=self.sum_tokenizer
                        )
                        #self.client_sock.send('SUM_GEN_OK-'.encode())
                    else:
                        print('didnt sum')
                        print('gpt out:',self.gpt_output)
                        self.sum_list = sum_api.just_split(self.gpt_output)
                        print('sum list:', self.sum_list)
                       # self.client_sock.send('SUM_GEN_OK-'.encode())
                    
                    #tmp = '|'.join(self.sum_list)
                    #self.client_sock.send(f'SUM_GEN_OK-'.encode())
                    
                elif recv_sparse[0] == 'DIFF_GEN':
                    model_id = recv_sparse[1]
                    print('diff model_id:',model_id)
                    
                    special_prompt = recv_sparse[2]
                    print(f'special_prompt:{special_prompt}')
                    
                    
                    num_process = recv_sparse[3]
                    print('num_process:', num_process)
                    
                    guidance_scale = recv_sparse[4]
                    print('guidance_scale:', guidance_scale)
                    
                    files = glob.glob(f'./users/{self.client_name}/*.png')
                    for f in files:
                        os.remove(f)
                    self.client_sock.setblocking(1)

                    print('server gening diff')
                    diff_api.diff_api_gen(
                        self.sum_list,
                        self.client_sock,
                        self.client_name,
                        special_prompt, 
                        model_id,
                        num_process,
                        guidance_scale,
                    )
                    print('server done gening')
                    time.sleep(2)
                    self.client_sock.send('DIFF_GEN_OK|'.encode())
                    self.client_sock.setblocking(0)
                
                elif recv_sparse[0] == 'UPLOAD':
                    user_name = recv_sparse[1]
                    print(f'user|{user_name} want to share')
                    
                    j=0
                    if os.listdir(f'./users/{user_name}'):
                        print(f'./users/{user_name} is not empty')
                        if not os.path.exists(f'./galary/{user_name}'):
                            os.mkdir(f'./galary/{user_name}')
                            
                        for i in range(0,100):
                            temp = str(i).rjust(3,'0')
                            if not os.path.exists(f'./galary/{user_name}/{user_name}_{temp}'):
                                os.mkdir(f'./galary/{user_name}/{user_name}_{temp}')
                                j=i
                                print(f'dest: ./galary/{user_name}/{user_name}_{temp}')
                                break
                    
                        for file in os.listdir(f'./users/{user_name}'):
                            print('file',file)
                            temp = str(j).rjust(3,'0')
                            shutil.copy2(f'./users/{user_name}/{file}', f'./galary/{user_name}/{user_name}_{temp}')
                
                elif recv_sparse[0] == 'GALARY_INIT':
                    self.galary_name_list = []
                    print('client req for galary story list')              
                    files = glob.glob(f'galary/*')
                    for f in files:
                        # print(f)
                        file_layer_2 = glob.glob(f'{f}\*')
                        for f2 in file_layer_2:
                            if os.path.isdir(f2):
                                filename = f2.rsplit('\\')[-1]
                                print('filename:',filename)
                                self.galary_name_list.append(filename)
                    full_galary_name = "@".join(self.galary_name_list)
                    self.client_sock.send(f'GALARY_NAME|{full_galary_name}'.encode())
                
                elif recv_sparse[0] == 'GALARY_REQ':
                    self.client_sock.setblocking(1)
                    galary_name = recv_sparse[1]
                    galary_user = galary_name.split('_')[0]
                    print(f'client req for galary story: {galary_name}')
                    self.client_sock.setblocking(1)
                    i = 0
                    files = glob.glob(f'./galary/{galary_user}/{galary_name}/*.png')
                    #files.sort(key=os.path.getmtime)
                    fileNames = sorted(files, key=lambda y: (y.rsplit('.', 1)[0]))
                    for f in fileNames:
                        print('客戶端要求的順序:', f)
                    try:
                        for f in files:
                            image_r =  open(f, "rb")
                            self.client_sock.send('GALARY_OUT|'.encode())
                            print(f'sending galary image:{f}')
                            while True:
                                imgData = image_r.readline(4096)
                                self.client_sock.send(imgData)
                                if not imgData:
                                    print('data clear')
                                    break  # 讀完檔案結束迴圈
                            image_r.close()
                            time.sleep(10)
                    except PIL.UnidentifiedImageError:
                        return 0
                        
                        
                    
                    print('galary done sending')
                    time.sleep(2)
                    
                    
                    try:
                        f = open(f'galary/{galary_user}/{galary_name}/story.txt', 'r', encoding='utf-8')
                    except Exception as e:
                        print(e)
                        
                    data = f.read()
                    self.galary_sum_list = sum_api.just_split([data])
                    
                    self.client_sock.send(f'GALARY_OUT_OK|{data}'.encode())
                    self.client_sock.setblocking(0)
                
                elif recv_sparse[0] == 'TRAN_INIT':
                    if self.tran_state == False:
                        if self.tran_lock == 0:
                            print('initing tran model')
                            self.tran_lock = 1
                            self.tran_model , self.tran_tokenizer, self.tran_lock = tran_api.get_model()
                            self.tran_state = True
                            self.client_sock.send("TRAN_CREATED|".encode())
                            
                elif recv_sparse[0] == 'TRAN_GEN':
                    print('sum_list:',self.sum_list)
                    idx = recv_sparse[2]
                    if len(self.sum_list) and self.tran_state:
                        print('start translate')
                        translated_text = tran_api.translate(self.sum_list[int(idx)], self.tran_model, self.tran_tokenizer)
                        self.client_sock.send(f'TRAN_OUT|{translated_text}'.encode())
                
                elif recv_sparse[0] == 'TRAN_GALARY_GEN':
                    print('galary_sum_list:',self.galary_sum_list)
                    idx = recv_sparse[2]
                    if len(self.galary_sum_list) > 0 and self.tran_state:
                        print('start galary translate')
                        translated_text = tran_api.translate(self.galary_sum_list[int(idx)], self.tran_model, self.tran_tokenizer)
                        self.client_sock.send(f'TRAN_GALARY_OUT|{translated_text}'.encode())
                    
            except:
                pass
            
    def check_alive(self):
        import time
        while True:
            time.sleep(10)
            try:
                check = self.client_sock.send('ALIVE?'.encode())
            except:
                print(f'client- {self.client_name} | is offline!')
                self.alive = False
                self.client_sock.close()
                break

