from transformers import AutoTokenizer, AutoModelForSeq2SeqLM

def get_model():
    tokenizer = AutoTokenizer.from_pretrained("facebook/m2m100_418M")
    model = AutoModelForSeq2SeqLM.from_pretrained("facebook/m2m100_418M")
    # device = 'cuda'
    # model.eval()
    # model.to(device)
    print('tran model created')
    return model, tokenizer, 0

def translate(text, model, tokenizer):
    print(f'prepared to translate:{text}')
    tokenizer.src_lang = 'en'
    encoded_text = tokenizer(text, return_tensors="pt")
    generated_tokens = model.generate(**encoded_text, forced_bos_token_id=tokenizer.get_lang_id("zh"))
    a = tokenizer.batch_decode(generated_tokens, skip_special_tokens=True)
    return a[0]
    
    
