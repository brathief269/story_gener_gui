import socket
from threading import *
from _thread import *
import threading
import select

from api import sum_api
class client():
    def __init__(self, user_name):
        self.host = '26.58.83.61'
        self.port = 5269
        self.user_name = user_name
        self.image_path = './client_out'
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        connect_check = self.s.connect_ex((self.host, self.port))
        self.s.setblocking(True)
        
        print('connect_check:',connect_check)
        print(self.s)
        print(self.s.fileno())
        
        self.galary_translated_text = None
        self.translated_text = None
        self.tran_state = False
        self.gpt_output = ''
        self.GPT_state = False
        self.GPT_out_state = False
        self.sum_state = False
        self.diff_state = False
        self.diff_gen_state = False
        self.sum_gen_state = False
        self.images_num = 0
        self.tmp_num = 0
        self.galary_name_list = []
        self.galary_story = ''
        self.picture_text = ''
        self.t_s = Thread(target=self.t_send,daemon=True)
        self.t_r = Thread(target=self.t_recv,daemon=True)
        
        event = threading.Event()
        
        
    def send_info(self):
        
        print('senting info')
        self.s.send((self.user_name).encode())
        print('info sent!')
    
        
        self.s.setblocking(False)
        self.t_s.start()
        self.t_r.start()
        
        
        
    def t_send(self):
        print('send thread')
        while True:
            
            try:
                send = self.s.send(input().encode())
                if send == 0:
                    self.s.close()
                    print(self.s)
                    print(self.s.fileno())
                    break
                print('clinet send:', send)
            except:
                pass
            
    def t_recv(self):
        print('recv thread')
        while True and self.s.fileno() != -1:
            try:
                recv = self.s.recv(99999).decode()
                print('client recv:',recv)
                recv_sparse = recv.split('|')
                if recv_sparse[0] == 'GPT_CREATED':
                    print('GPT_CREATED')
                    self.GPT_state = True
                    
                if recv_sparse[0] == 'GPT_GEN_OK':
                    self.gpt_output = recv[11:]
                    with open(f'{self.image_path}/story.txt','w',encoding='utf-8') as f:
                        f.write(self.gpt_output)
                        f.close()
                    print('client-recv-story:', self.gpt_output)
                    self.GPT_out_state = True
                
                # if recv_sparse[0] == 'SUM_CREATED':
                #     self.sum_state = True
                #     print('SUM_CREATED')
                    
                
                # if recv_sparse[0] == 'SUM_GEN_OK':
                #     self.sum_gen_state = True
                #     self.picture_text = recv_sparse[1]
                #     self.sum_list = sum_api.just_split(self.gpt_output)
                    
                #     print(f'sum gen ok:{self.sum_list}')
                    
                if recv_sparse[0] == 'DIFF_CREATED':
                    print('DIFF_CREATED')
                    self.diff_state = True
                
                if recv_sparse[0] == 'DIFF_GEN_OK':
                    self.diff_gen_state = True
                    print('diff gen ok')
                
                if recv_sparse[0] == 'DIFF_OUT':
                    self.s.setblocking(1)
                    print('receiving imgs')
                    temp = str(self.images_num).rjust(3,'0')
                    print('paddding_num:',temp)
                    imgFile = open(
                        f'{self.image_path}/diff_out_{temp}.png',
                        'wb'
                    )# 開始寫入圖片檔
                    
                    while True:
                        ready = select.select([self.s],[],[],1)
                        if ready[0]:
                            imgData = self.s.recv(4096)  # 接收遠端主機傳來的數據
                            imgFile.write(imgData)
                        else:
                            print('no data')
                            break
                    imgFile.close()
                    print(f'client recv img: {self.images_num} done')
                    self.images_num+=1
                    self.s.setblocking(0)
                    
                if recv_sparse[0] == 'GALARY_NAME':
                    print(f'galary name:{recv_sparse[1]}')
                    g_name = recv_sparse[1].split('@')
                    for g in g_name:
                        print('g:',g)
                        self.galary_name_list.append(g)
            
                if recv_sparse[0] == 'GALARY_OUT':
                    self.galary_state = False
                    self.s.setblocking(1)
                    print('receiving galary imgs')
                    temp = str(self.tmp_num).rjust(3,'0')
                    imgFile = open(
                        f'./client_tmp/diff_out_{temp}.png',
                        'wb'
                    )  # 開始寫入圖片檔
                    while True:
                        ready = select.select([self.s],[],[],2)
                        if ready[0]:
                            imgData = self.s.recv(4096)  # 接收遠端主機傳來的數據
                            imgFile.write(imgData)
                        else:
                            print('no data')
                            break
                    imgFile.close()
                    print(f'client recv (galary) img: {self.tmp_num} done')
                    self.tmp_num+=1
                    self.s.setblocking(0)
                
                if recv_sparse[0] == 'GALARY_OUT_OK':
                    self.galary_story = recv[14:]
                    print('galary_story:',self.galary_story)
                    f = open(
                        './client_tmp/story.txt',
                        'w', encoding='utf-8'
                    )
                    f.write(self.galary_story)
                    f.close()
                    self.galary_state = True
                
                if recv_sparse[0] == 'TRAN_CREATED':
                    print('TRAN_CREATED')
                    self.tran_state = True
                
                if recv_sparse[0] == 'TRAN_OUT':
                    self.translated_text = recv_sparse[1]
                    print('翻譯完成: ',self.translated_text)
                
                if recv_sparse[0] == 'TRAN_GALARY_OUT':
                    self.galary_translated_text = recv_sparse[1]
                    
            except:
                pass

    
    # c.t_s.start()
    # c.t_r.start()
    