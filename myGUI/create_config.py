import json, os

if __name__ == '__main__':
    
    article_info = {
        'max_length' : 550,
        'min_length' : 20, 
        'top_k' : 50,
        'do_sample' : True,
    }
    myJSON = json.dumps(article_info)

    with open("api\gpt_config\GPT_grim_10_5e-5.json", "w") as jsonfile:
        jsonfile.write(myJSON)
        print("Write successful")